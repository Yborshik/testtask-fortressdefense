﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour {
    private Transform _transform;

    public bool IsCellEmpty
    {
        get
        {
            if (Transform.childCount == 0)
                return true;
            else
                return false;
        }
    }

    public Transform Transform
    {
        get
        {
            if (_transform == null)
                _transform = GetComponent<Transform>();
            return _transform;
        }

        private set
        {
            _transform = value;
        }
    }

    /// <summary>
    /// Установить юнит в ячейку
    /// </summary>
    /// <param name="unit">Юнит</param>
    /// <returns>True - если юнит успешно установлен, False - если ячейка пуста</returns>
    public bool SetUnit(GameObject unit)
    {
        if (IsCellEmpty)
        {
            unit.transform.SetParent(Transform);
            //TODO: Сделать скейл персонажа в зависимости от дальности от нижнего экрана

            float scale = GameManager.Instance.Grid.GetLine(this).ScaleHumaniod;
            unit.transform.localScale = new Vector3(scale, scale);

            transform.position = new Vector3(transform.position.x, transform.position.y, GameManager.Instance.Grid.GetIndexLine(this));
            return true;
        }
        else
        {
            Destroy(unit);
            return false;
        }
    }
}
