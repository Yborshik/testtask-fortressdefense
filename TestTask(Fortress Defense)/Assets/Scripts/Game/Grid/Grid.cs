﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour {
    [SerializeField]
    private List<GridLine> _lines = new List<GridLine>();

    private List<Cell> _allCells;

    /// <summary>
    /// Все ячейки на поле
    /// </summary>
    public List<Cell> AllCells
    {
        get
        {
            if(_allCells == null)
            {
                _allCells = new List<Cell>();

                foreach (var line in _lines)
                    foreach (var cell in line.Cells)
                        _allCells.Add(cell);
            }
            return _allCells;
        }
    }

    public GridLine GetLine(Cell cell)
    {
        for (int i = 0; i < _lines.Count; i++)
        {
            if (_lines[i].Contains(cell))
                return _lines[i];
        }

        return null;
    }

    public int GetIndexLine(Cell cell)
    {
        GridLine gridLine = GetLine(cell);
        return _lines.IndexOf(gridLine);
    }

    public int GetIndexLine(GridLine gridLine)
    {
        return _lines.IndexOf(gridLine);
    }

    public GridLine GetRandomLine()
    {
        return _lines[Random.Range(0, _lines.Count)];
    }
}
