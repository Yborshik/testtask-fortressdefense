﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GridLine : MonoBehaviour {

    public UnityEvent OnEnemiesEmpty = new UnityEvent();
    public UnityEvent OnEnemiesSpawned = new UnityEvent();

    [SerializeField]
    private float _scaleHumanoid = 1;

    private int _childCount = 0;

    [SerializeField]
    private Transform _enemies;

    [SerializeField]
    private List<Cell> _cells = new List<Cell>();

    /// <summary>
    /// Размер юнита для этой линии
    /// </summary>
    public float ScaleHumaniod
    {
        get
        {
            return _scaleHumanoid;
        }
    }

    public Transform Enemies
    {
        get
        {
            return _enemies;
        }
    }

    public List<Cell> Cells
    {
        get
        {
            return _cells;
        }
    }

    public int GetCellIndex(Cell cell)
    {
        if (_cells.Contains(cell))
            return _cells.IndexOf(cell);
        else
        {
            print("Cell " + cell.name + " not find int Line: " + name);
            return -1;
        }
    }

    public bool Contains(Cell cell)
    {
        if (_cells.Contains(cell))
            return true;
        else
            return false;
    }

    public void Update()
    {
        CheckEnemyCount();
    }

    private void CheckEnemyCount()
    {
        int childCount = transform.childCount;

        if(childCount != _childCount)
        {
            if (childCount == 0)
                OnEnemiesEmpty.Invoke();
            else if (_childCount == 0)
                OnEnemiesSpawned.Invoke();

            _childCount = childCount;
        }
    }
}
