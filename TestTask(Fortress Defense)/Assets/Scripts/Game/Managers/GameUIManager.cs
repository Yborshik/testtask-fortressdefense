﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUIManager : MonoBehaviour
{
    [SerializeField]
    private ScrollRect _buyUnitsMenu;
    [SerializeField]
    private GameObject _buyUnitsMenuItem;
    [SerializeField]
    private Text _points;

    [SerializeField]
    private GameObject _win;
    [SerializeField]
    private GameObject _loose;

	// Use this for initialization
	void Start () {
        FillBuyUnitsMenu();
	}
	
	// Update is called once per frame
	void Update () {
        DrawPoints();
	}

    private void FillBuyUnitsMenu()
    {
        foreach (var item in GameManager.Instance.AvaliableUnits)
        {
            Instantiate(_buyUnitsMenuItem, _buyUnitsMenu.content).GetComponent<BuyUnitsMenuItem>().Initialize(item);
        }
    }

    private void DrawPoints()
    {
        _points.text = "Points: " + GameManager.Instance.Points.ToString();
    }

    public void ShowLoose()
    {
        _loose.SetActive(true);
    }

    public void ShowWin()
    {
        _win.SetActive(true);
    }

    public void Rstart()
    {
        GameSceneManager.Load(SceneName.GAME);
    }

    public void BackToMenu()
    {
        GameSceneManager.Load(SceneName.MENU);
    }

    public void Continue()
    {
        GameSceneManager.Load(SceneName.GAME);
    }
}
