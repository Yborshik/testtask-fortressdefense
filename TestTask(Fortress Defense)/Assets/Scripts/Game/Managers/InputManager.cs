﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputManager : MonoBehaviour
{

    /// <summary>
    /// Когда 1 палец прикоснулся к экрану
    /// </summary>
    public static UnityOneTouchEvent OnOneFingerStart = new UnityOneTouchEvent();
    /// <summary>
    /// Когда 1 палец в движении
    /// </summary>
    public static UnityOneTouchEvent OnOneFingerMove = new UnityOneTouchEvent();
    /// <summary>
    /// Когда 1 палец убран с экрана либо пользователь добавил второй
    /// </summary>
    public static UnityEvent OnOneFingerEnd = new UnityEvent();

    private Touch _lastFakeTouch = new Touch();

    // Update is called once per frame
    void Update()
    {
        AndroidInputs();
        EditorInputs();
    }

    private void AndroidInputs()
    {
        if (Input.touchCount == 1)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                OnOneFingerStart.Invoke(Input.GetTouch(0));
            }

            if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                OnOneFingerMove.Invoke(Input.GetTouch(0));
            }

            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                OnOneFingerEnd.Invoke();
            }
        }
    }

    private void EditorInputs()
    {
        FakeTouchInput faketouch = FakeTouch();

        if (faketouch == null)
            return;

        if (faketouch.touch.phase == TouchPhase.Began)
        {
            OnOneFingerStart.Invoke(faketouch.touch);
        }

        if (faketouch.touch.phase == TouchPhase.Moved)
        {
            OnOneFingerMove.Invoke(faketouch.touch);
        }

        if (faketouch.touch.phase == TouchPhase.Ended)
        {
            OnOneFingerEnd.Invoke();
        }
    }

    private FakeTouchInput FakeTouch()
    {
        FakeTouchInput faketouch = new FakeTouchInput();
        Touch touch = new Touch();
        if (Input.GetMouseButtonDown(0))
        {
            touch.phase = TouchPhase.Began;
            touch.deltaPosition = new Vector2(0, 0);
            touch.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            touch.fingerId = 0;

            faketouch.touch = touch;
            _lastFakeTouch = touch;

            return faketouch;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            touch.phase = TouchPhase.Ended;
            Vector2 newPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            touch.deltaPosition = newPosition - _lastFakeTouch.position;
            touch.position = newPosition;
            touch.fingerId = 0;

            faketouch.touch = touch;
            _lastFakeTouch = touch;

            return faketouch;
        }
        else if (Input.GetMouseButton(0))
        {
            touch.phase = TouchPhase.Moved;
            Vector2 newPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            touch.deltaPosition = newPosition - _lastFakeTouch.position;
            touch.position = newPosition;
            touch.fingerId = 0;

            faketouch.touch = touch;
            _lastFakeTouch = touch;

            return faketouch;
        }
        else
        {
            return faketouch = null;
        }
    }
}

public class UnityOneTouchEvent : UnityEvent<Touch>
{

}

public class FakeTouchInput
{
    public Touch touch;
}