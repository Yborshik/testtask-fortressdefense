﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Singleton

    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GameManager>();
            return _instance;
        }
    }

    #endregion

    [SerializeField]
    private Grid _grid;
    [SerializeField]
    private EnemySpawner _enemySpawner;
    [SerializeField]
    private Fortress _fortress;
    [SerializeField]
    private GameUIManager _gameUIManager;

    [SerializeField]
    /// <summary>
    /// Доступные для покупки юниты
    /// </summary>
    private List<GameObject> _avaliableUnits = new List<GameObject>();

    [Range(0, 10)]
    /// <summary>
    /// Кол-во поинтов получаемое каждую секунду
    /// </summary>
    [SerializeField]
    private int _pointsPerSecond = 1;

    [SerializeField]
    private int _points;

    public Grid Grid
    {
        get
        {
            return _grid;
        }
    }

    /// <summary>
    /// Валюта для покупки юнитов
    /// </summary>
    public int Points
    {
        get
        {
            return _points;
        }

        set
        {
            _points = value;
        }
    }

    public List<GameObject> AvaliableUnits
    {
        get
        {
            return _avaliableUnits;
        }
    }

    private void Start()
    {
        _fortress.OnFortressDeath.AddListener(GameOver);
        _enemySpawner.OnAllEnemiesDie.AddListener(Win);

        StartCoroutine(PointsPerSecond());

        _enemySpawner.StartSpawn();
    }

    public void GameOver()
    {
        _gameUIManager.ShowLoose();
    }

    public void Win()
    {
        _gameUIManager.ShowWin();
    }

    private IEnumerator PointsPerSecond()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            Points += _pointsPerSecond;
        }
    }
}
