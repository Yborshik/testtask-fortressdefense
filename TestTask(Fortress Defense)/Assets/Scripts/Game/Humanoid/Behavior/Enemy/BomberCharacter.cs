﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BomberCharacter : EnemyCharacter
{
    void Awake()
    {
        IdleState = new EnemyIdleState();
        WalkState = new EnemyWalkState();
        AttackState = new EnemyAttackState();
        DeathState = new EnemyDeathState();
    }
}
