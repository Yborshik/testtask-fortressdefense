﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitCharacter : CharacterBase, IDragable
{
    /// <summary>
    /// Стоимоть юнита
    /// </summary>
    public int price;

    private float _magnetDistanceToCell = 2f;

    /// <summary>
    /// Все ячейки на поле
    /// </summary>
    private List<Cell> _cells = new List<Cell>();

    private Cell _targetCell;

    /// <summary>
    /// Ячейка в которой находится юнит
    /// </summary>
    public Cell Cell
    {
        get
        {
            return GetComponentInParent<Cell>();
        }
    }

    public void InitDrag()
    {
        InputManager.OnOneFingerMove.AddListener(Drag);
        InputManager.OnOneFingerEnd.AddListener(EndDrag);

        _cells = GameManager.Instance.Grid.AllCells;
    }

    public void Drag(Touch touch)
    {
        Vector3 screenToWorldPoint = Camera.main.ScreenToWorldPoint(touch.position);
        Vector3 newPosition = new Vector3(screenToWorldPoint.x, screenToWorldPoint.y, 0);

        //Найти расстояния до каждой ближней ячейки
        Dictionary<Cell, float> distances = new Dictionary<Cell, float>();
        foreach (var cell in _cells)
        {
            float distanceToCell = Vector2.Distance(cell.Transform.position, newPosition);
            if (distanceToCell < _magnetDistanceToCell)
                distances.Add(cell, distanceToCell);
        }

        //Найти ближайшую ячейку
        _targetCell = null;
        foreach (var item in distances)
        {
            if (_targetCell == null)
            {
                _targetCell = item.Key;
                continue;
            }

            if (item.Value < distances[_targetCell])
                _targetCell = item.Key;
        }

        //Если есть рядом ячейка, магнитим юнита к ней
        if (_targetCell != null)
        {
            transform.position = _targetCell.Transform.position;
        }
        else
            transform.position = newPosition;
    }

    public void EndDrag()
    {
        InputManager.OnOneFingerMove.RemoveListener(Drag);
        InputManager.OnOneFingerEnd.RemoveListener(EndDrag);

        if (_targetCell == null)
        {
            Destroy(gameObject);
            GameManager.Instance.Points += price;
        }
        else if (!_targetCell.SetUnit(gameObject))
            GameManager.Instance.Points += price;
    }
}
