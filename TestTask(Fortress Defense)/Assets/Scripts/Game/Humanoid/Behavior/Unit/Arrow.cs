﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {

    private Transform _transform;
    private Rigidbody2D _rigidbody2D;
    private SpriteRenderer _spriteRenderer;

    [SerializeField]
    private float _speed = 4;
    private float _damage = 0;

    private CharacterBase _characterBase;

	// Use this for initialization
	void Start () {
        _transform = GetComponent<Transform>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();

        Destroy(gameObject, 10f);
    }
	
	// Update is called once per frame
	void Update () {
        ArrowMoving();
	}

    public void Initialize(CharacterBase characterBase)
    {
        _characterBase = characterBase;
        _damage = characterBase.damage;
    }

    private void ArrowMoving()
    {
        Vector2 movement = - _transform.right * _speed * Time.deltaTime;

        _rigidbody2D.MovePosition(_rigidbody2D.position + movement);
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        IDamageble damageble = collider.GetComponent<IDamageble>();
        if (damageble != null)
        {
            CharacterBase characterBase = damageble as CharacterBase;
            if (characterBase.IsUnit)
                return;
            else
            {
                Grid grid = GameManager.Instance.Grid;

                //Если линия врага совпадает с линией юнита - атакуем его
                if (grid.GetIndexLine(characterBase.GridLine) != grid.GetIndexLine(characterBase.GridLine))
                    return;

                damageble.TakeDamage(_damage);
                Destroy(gameObject);
            }
        }
    }
}
