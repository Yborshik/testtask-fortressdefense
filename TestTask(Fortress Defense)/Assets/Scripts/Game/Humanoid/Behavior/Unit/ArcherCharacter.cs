﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcherCharacter : UnitCharacter
{
    public Transform spawnArrowPoint;

    public GameObject arrowPrefab;

    public int frameOfArrowOutput;

    private void Awake()
    {
        IdleState = new UnitIdleState();
        AttackState = new UnitAttackState();
        DeathState = new UnitDeathState();
    }
}
