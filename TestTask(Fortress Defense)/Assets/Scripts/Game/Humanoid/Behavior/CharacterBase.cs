﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

public class CharacterBase : MonoBehaviour, IDamageble
{
    public OnDestroyUnityEvent OnGameObjectDestroy = new OnDestroyUnityEvent();

    /// <summary>
    /// Урон который наносит юнит
    /// </summary>
    public float damage;
    /// <summary>
    /// Перезарядка между атаками в секундах
    /// </summary>
    public float reloadingTime;

    [SerializeField]
    private float _health;

    public float Health
    {
        get
        {
            return _health;
        }
        private set
        {
            _health = value;
            if (_health < 0)
            {
                _health = 0;
                State = DeathState;
            }
            else
            {
                //TODO: изменить на дотвин
                StartCoroutine(DamageFlicker());
            }
        }
    }

    public bool IsEnemy { get { return this is EnemyCharacter; } }

    public bool IsUnit { get { return this is UnitCharacter; } }

    #region Components

    private Rigidbody2D _rigidbody2D;
    private Collider2D _collider2D;
    private Animator _animator;
    private AudioSource _mainAudioSource;

    /// <summary>
    /// Rigidbody component
    /// </summary>
    public Rigidbody2D Rigidbody2D
    {
        get
        {
            if (_rigidbody2D == null)
                _rigidbody2D = GetComponent<Rigidbody2D>();
            return _rigidbody2D;
        }
    }

    /// <summary>
    /// Main Collider component
    /// </summary>
    public Collider2D Collider2D
    {
        get
        {
            if (_collider2D == null)
                _collider2D = GetComponent<Collider2D>();
            return _collider2D;
        }
    }

    /// <summary>
    /// Animator component
    /// </summary>
    public Animator Animator
    {
        get
        {
            if (_animator == null)
                _animator = GetComponent<Animator>();
            return _animator;
        }
    }

    /// <summary>
    /// Main AudioSource component
    /// </summary>
    public AudioSource MainAudioSource
    {
        get
        {
            if (_mainAudioSource == null)
                _mainAudioSource = GetComponent<AudioSource>();
            return _mainAudioSource;
        }
    }

    /// <summary>
    /// Линия на которой находится гуманоид
    /// </summary>
    public GridLine GridLine
    {
        get { return GetComponentInParent<GridLine>(); }
    }

    #endregion

    #region states

    private CharacterState _state;

    private CharacterState _idleState;
    private CharacterState _walkState;
    private CharacterState _deathState;
    private CharacterState _attackState;

    public CharacterState IdleState
    {
        get { return _idleState; }
        protected set { _idleState = value; }
    }

    public CharacterState WalkState
    {
        get { return _walkState; }
        protected set { _walkState = value; }
    }

    public CharacterState DeathState
    {
        get { return _deathState; }
        protected set { _deathState = value; }
    }

    public CharacterState AttackState
    {
        get { return _attackState; }
        set { _attackState = value; }
    }

    /// <summary>
    /// Состояние персонажа
    /// </summary>
    public CharacterState State
    {
        get
        {
            return _state;
        }
        set
        {
            _state = value;
            _state.Enter(this);
        }
    }

    #endregion

    public void TakeDamage(float damage)
    {
        Health -= damage;


    }

    /// <summary>
    /// Когда юнит получает урон - окрашивать картинку в красный цвет
    /// </summary>
    private IEnumerator DamageFlicker()
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();

        spriteRenderer.color = new Color32(255, 201, 201, 255);

        yield return new WaitForSeconds(0.1f);

        spriteRenderer.color = new Color32(255, 255, 255, 255);
    }

    private void OnDestroy()
    {
        OnGameObjectDestroy.Invoke(gameObject);
;    }
}

public class OnDestroyUnityEvent : UnityEvent<GameObject>
{

}