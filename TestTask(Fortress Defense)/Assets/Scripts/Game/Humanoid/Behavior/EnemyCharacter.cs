﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCharacter : CharacterBase
{
    /// <summary>
    /// Дальность атаки
    /// </summary>
    public float attackRange;
    /// <summary>
    /// Скорость юнита
    /// </summary>
    public float speed;
}
