﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanoidController : MonoBehaviour {

    public CharacterBase Character { get; private set; }

    // Use this for initialization
    void Start () {
        Character = GetComponent<CharacterBase>();
        Character.State = Character.IdleState;
    }

    // Update is called once per frame
    void Update () {
        Character.State.Update(Character);
	}
}