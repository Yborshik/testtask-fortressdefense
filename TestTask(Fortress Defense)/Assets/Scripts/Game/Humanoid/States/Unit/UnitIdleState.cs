﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitIdleState : CharacterState {

    public override void Enter(CharacterBase character)
    {
        character.Animator.Play("Idle");
    }

    public override void Update(CharacterBase character)
    {
        if (character.AttackState.IsReady(character))
            character.State = character.AttackState;
    }
}
