﻿using UnityEngine;

/// <summary>
/// Состояние атаки противника
/// </summary>
public class UnitAttackState : CharacterState
{
    private float _reloadingEnds;

    public override void Enter(CharacterBase character)
    {
        ArcherCharacter archerCharacter = character as ArcherCharacter;

        character.Animator.Play("Shoot");
        //TODO: Extention Method
        //character.Animator.SetFloat("speed", )
        //character.Animator.runtimeAnimatorController.animationClips

        AnimationClip clip = new AnimationClip();
        foreach (var item in archerCharacter.Animator.runtimeAnimatorController.animationClips)
        {
            if (item.name == "Shoot")
            {
                clip = item;
                archerCharacter.reloadingTime = item.length;
            }
        }

        //Расчет времени когда выпускать стрелу
        _reloadingEnds = Time.time + (archerCharacter.reloadingTime / clip.frameRate) * archerCharacter.frameOfArrowOutput;
    }

    public override void Update(CharacterBase character)
    {
        ArcherCharacter archerCharacter = character as ArcherCharacter;

        if (archerCharacter.GridLine.Enemies.childCount == 0)
            archerCharacter.State = archerCharacter.IdleState;

        if (_reloadingEnds < Time.time)
        {
            Attack(archerCharacter);
            _reloadingEnds += archerCharacter.reloadingTime;
        }
    }

    public override bool IsReady(CharacterBase character)
    {
        Cell cell = character.GetComponentInParent<Cell>();

        if (cell == null)
            return false;

        if ((character as UnitCharacter).GridLine.Enemies.childCount == 0)
            return false;
            

        //Collider2D[] collider2D = Physics2D.OverlapCircleAll(character.transform.position, 1 << LayerMask.NameToLayer("Unit"));

        //if (collider2D.Length > 0)
        //    return true;
        //else
        //    return false;
        return true;
    }

    private void Attack(ArcherCharacter archerCharacter)
    {
        GameObject arrow = GameObject.Instantiate(archerCharacter.arrowPrefab);
        arrow.transform.position = archerCharacter.spawnArrowPoint.position;
        arrow.transform.localScale = archerCharacter.transform.localScale;

        arrow.GetComponent<SpriteRenderer>().sortingOrder = archerCharacter.GetComponent<SpriteRenderer>().sortingOrder;

        arrow.GetComponent<Arrow>().Initialize(archerCharacter);
    }
}
