﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitDeathState : CharacterState
{
    public override void Enter(CharacterBase character)
    {
        GameObject.Destroy(character.gameObject);
    }

    public override void Update(CharacterBase character)
    {
        
    }
}
