﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CharacterState {

    /// <summary>
    /// Инициализация состояния, применение всех параметров для состояния
    /// </summary>
    /// <param name="character"></param>
    public abstract void Enter(CharacterBase character);

    /// <summary>
    /// Метод обновления состояния
    /// </summary>
    /// <param name="character"></param>
    public abstract void Update(CharacterBase character);

    /// <summary>
    /// Готово ли состояние к переходу на него
    /// </summary>
    /// <param name="character"></param>
    /// <returns></returns>
    public virtual bool IsReady(CharacterBase character)
    {
        return true;
    }
}
