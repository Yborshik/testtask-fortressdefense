﻿using UnityEngine;

/// <summary>
/// Состояние атаки противника
/// </summary>
public class EnemyAttackState : CharacterState
{
    private float _reloadingEnds;

    public override void Enter(CharacterBase character)
    {
        EnemyCharacter enemyCharacter = character as EnemyCharacter;

        character.Animator.Play("Attack");

        //TODO: сделать скорость анимации в зависимости от скорости перезарядки

        foreach (var item in enemyCharacter.Animator.runtimeAnimatorController.animationClips)
        {
            if (item.name == "Attack")
            {
                enemyCharacter.reloadingTime = item.length;
            }
        }

        // reloadingTime/2 что бы урон наносился с анимацией
        _reloadingEnds = Time.time + enemyCharacter.reloadingTime/2;//enemyCharacter.reloadingTime
    }

    public override void Update(CharacterBase character)
    {
        EnemyCharacter enemyCharacter = character as EnemyCharacter;

        IDamageble damageble = CheckDamagebleObjects(enemyCharacter);

        if (damageble == null)
            enemyCharacter.State = enemyCharacter.WalkState;

        if (_reloadingEnds < Time.time)
        {
            damageble.TakeDamage(enemyCharacter.damage);
            _reloadingEnds += enemyCharacter.reloadingTime;
        }
    }


    public override bool IsReady(CharacterBase character)
    {
        EnemyCharacter enemyCharacter = character as EnemyCharacter;

        IDamageble damageble = CheckDamagebleObjects(enemyCharacter);

        if (damageble == null)
            return false;
        else
            return true;
    }

    private IDamageble CheckDamagebleObjects(EnemyCharacter enemyCharacter)
    {
        Collider2D[] collider2D = Physics2D.OverlapCircleAll(enemyCharacter.transform.position, enemyCharacter.attackRange);

        for (int i = 0; i < collider2D.Length; i++)
        {
            IDamageble damageble = collider2D[i].GetComponent<IDamageble>();
            if (damageble != null)
            {
                CharacterBase characterBase = damageble as CharacterBase;
                if (characterBase != null)
                {
                    if (characterBase.IsUnit)
                    {
                        Grid grid = GameManager.Instance.Grid;
                        //Если линия врага совпадает с линией юнита - атакуем его
                        if (grid.GetIndexLine(enemyCharacter.GridLine) != grid.GetIndexLine(characterBase.GridLine))
                            return null;

                        return damageble;
                    }
                }
                else
                    return damageble;
            }
        }

        return null;
    }
}
