﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWalkState : CharacterState
{
    public override void Enter(CharacterBase character)
    {
        character.Animator.Play("Walk");
    }

    public override void Update(CharacterBase character)
    {
        EnemyCharacter enemyCharacter = character as EnemyCharacter;

        if (enemyCharacter.AttackState.IsReady(enemyCharacter))
            enemyCharacter.State = enemyCharacter.AttackState;

        Vector2 movement = enemyCharacter.transform.right * enemyCharacter.speed * Time.deltaTime;

        enemyCharacter.Rigidbody2D.MovePosition(enemyCharacter.Rigidbody2D.position + movement);
    }
}
