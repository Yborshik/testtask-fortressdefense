﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyIdleState : CharacterState {

    public override void Enter(CharacterBase character)
    {
        character.State = character.WalkState;
    }

    public override void Update(CharacterBase character)
    {

    }
}
