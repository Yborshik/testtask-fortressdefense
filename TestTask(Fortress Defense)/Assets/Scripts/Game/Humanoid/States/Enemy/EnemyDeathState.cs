﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeathState : CharacterState
{
    public override void Enter(CharacterBase character)
    {
        character.Animator.Play("Death");

        character.transform.SetParent(null);

        character.GetComponent<Collider2D>().enabled = false;

        GameObject.Destroy(character.gameObject, 5f);
    }

    public override void Update(CharacterBase character)
    {
        
    }
}
