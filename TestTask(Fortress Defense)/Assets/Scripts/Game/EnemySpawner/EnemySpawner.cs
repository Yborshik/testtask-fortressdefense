﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemySpawner : MonoBehaviour {

    /// <summary>
    /// Выполняется когда игрок убил всех зомби
    /// </summary>
    public UnityEvent OnAllEnemiesDie = new UnityEvent();

    /// <summary>
    /// Задержка между волнами
    /// </summary>
    [SerializeField]
    private float _waveDelay = 5;

    private int _spawnedEnemySortingOrder = -1;

    private bool _allEnemiesInWaveSpawned = false;

    [SerializeField]
    private List<Wave> _waves = new List<Wave>();
    /// <summary>
    /// Список живых врагов
    /// </summary>
    private List<GameObject> _aliveEnemies = new List<GameObject>();

    public void StartSpawn()
    {
        StartCoroutine(WaveSpawner());
    }

    private IEnumerator WaveSpawner()
    {
        foreach (var wave in _waves)
            yield return StartCoroutine(SpawnWawe(wave));

        StartCoroutine(CheckAliveEnemies());
    }

    /// <summary>
    /// Спавн противников в текущей волне
    /// </summary>
    /// <returns></returns>
    private IEnumerator SpawnWawe(Wave wave)
    {
        //Сумма всех врагов в волне
        int sumEnemyCount = WaveEnemySumCount(wave);

        _allEnemiesInWaveSpawned = false;

        //Спавн рандомного врага в волне на рандомной линии
        for (int i = 0; i < sumEnemyCount; i++)
        {
            //Список доступных для спавна врагов
            List<Wave.Enemy> enableEnemy = new List<Wave.Enemy>();
            foreach (var enemy in wave.enemies)
                if (enemy.count != 0)
                    enableEnemy.Add(enemy);

            //Взять рандомного врага из доступных
            Wave.Enemy randomEnemy = enableEnemy[Random.Range(0, enableEnemy.Count)];

            //Взять рандомную линию
            GridLine gridLine = GameManager.Instance.Grid.GetRandomLine();

            //Спавн врага
            GameObject spawnedEnemy = Instantiate(randomEnemy.prefab, gridLine.Enemies);
            spawnedEnemy.GetComponent<EnemyCharacter>().OnGameObjectDestroy.AddListener(OnEnemyDieHandler);
            _aliveEnemies.Add(spawnedEnemy);

            //Обнулить позицию усли у префаба не нулевые координаты
            spawnedEnemy.transform.localPosition = Vector3.zero;
            //Противники в полтора раза больше чем юниты, поэтому уменьшаем их
            spawnedEnemy.transform.localScale = new Vector3(gridLine.ScaleHumaniod/1.5f, gridLine.ScaleHumaniod/1.5f, 1);

            //Задать вразу очередь рендеринга в зависимости от линии (на данный момент на линию предусмотрено 100 словев)
            spawnedEnemy.GetComponent<SpriteRenderer>().sortingOrder = (_spawnedEnemySortingOrder) - (GameManager.Instance.Grid.GetIndexLine(gridLine) * 100);

            //Уменьшаем SortingOrder что бы враги рендерились корректно
            _spawnedEnemySortingOrder--;

            yield return new WaitForSeconds(wave.spawnDelay);
        }

        _allEnemiesInWaveSpawned = true;
    }

    /// <summary>
    /// Сумма всех врагов в текущей волне
    /// </summary>
    private int WaveEnemySumCount(Wave wave)
    {
        int sumEnemyCount = 0;

        foreach (var enemy in wave.enemies)
            sumEnemyCount += enemy.count;
        return sumEnemyCount;
    }

    private IEnumerator CheckAliveEnemies()
    {
        while(_aliveEnemies.Count > 0 || !_allEnemiesInWaveSpawned)
            yield return new WaitForEndOfFrame();

        OnAllEnemiesDie.Invoke();
    }

    private void OnEnemyDieHandler(GameObject enemy)
    {
        _aliveEnemies.Remove(enemy);
    }
}
