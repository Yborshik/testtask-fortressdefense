﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Wave
{
    public List<Enemy> enemies = new List<Enemy>();

    /// <summary>
    /// Время между спавнами противников
    /// </summary>
    public float spawnDelay;

    [System.Serializable]
    public class Enemy
    {
        public GameObject prefab;
        public int count;
    }
}
