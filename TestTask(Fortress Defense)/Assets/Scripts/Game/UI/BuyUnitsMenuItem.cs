﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BuyUnitsMenuItem : MonoBehaviour, IPointerDownHandler
{
    [SerializeField]
    private Image _preview;

    [SerializeField]
    private Text _price;

    private GameObject _prefab;

    /// <summary>
    /// Инициализация пункта покупки юнита
    /// </summary>
    /// <param name="unit">Юнит</param>
    public void Initialize(GameObject unit)
    {
        _prefab = unit;

        SpriteRenderer spriteRenderer = _prefab.GetComponent<SpriteRenderer>();

        _preview.sprite = spriteRenderer.sprite;

        _price.text = _prefab.GetComponent<UnitCharacter>().price.ToString();
    }

    /// <summary>
    /// Начать покупку если пользователь нажал на юнита в меню покупки
    /// </summary>
    public void OnPointerDown(PointerEventData eventData)
    {
        int unitPrice = int.Parse(_price.text);
        if (GameManager.Instance.Points > unitPrice)
            GameManager.Instance.Points -= unitPrice;
        else
            return;

        GameObject unit =  Instantiate(_prefab);
        Vector3 screenToWorldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 newPosition = new Vector3(screenToWorldPoint.x, screenToWorldPoint.y, 0);
        unit.transform.position = newPosition;

        IDragable dragable = unit.GetComponent<IDragable>();
        dragable.InitDrag();
    }
}
