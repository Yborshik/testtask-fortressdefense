﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Логика поведения крепости
/// </summary>
public class Fortress : MonoBehaviour, IDamageble
{
    public UnityEvent OnFortressDeath = new UnityEvent();

    private SpriteRenderer _spriteRenderer;

    [SerializeField]
    private List<Sprite> _damageView = new List<Sprite>();

    [SerializeField]
    private float _health = 100;
    /// <summary>
    /// Сколько всего здоровья у крепости
    /// </summary>
    private float _fullHealth;
    /// <summary>
    /// Шаг изменения состояния крепости, зависит от количества состояний крепости
    /// </summary>
    private float _healthStep;

    private bool isDead = false;

    private EdgeCollider2D _collider;

    public EdgeCollider2D Collider
    {
        get
        {
            if (_collider == null)
                _collider = GetComponent<EdgeCollider2D>();
            return _collider;
        }
    }

    public float Health
    {
        get
        {
            return _health;
        }
        private set
        {
            _health = value;
            if (_health < 0)
            {
                _health = 0;
                Death();
            }
            else
                StartCoroutine(DamageFlicker());

            UpdateDamageView();
        }
    }

    private void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();

        _fullHealth = Health;
        _healthStep = _fullHealth / (_damageView.Count - 1);

        UpdateDamageView();
    }

    public void TakeDamage(float damage)
    {
        Health -= damage;
    }

    /// <summary>
    /// Когда юнит получает урон - окрашивать картинку в красный цвет
    /// </summary>
    private IEnumerator DamageFlicker()
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();

        spriteRenderer.color = new Color32(255, 201, 201, 255);

        yield return new WaitForSeconds(0.1f);

        spriteRenderer.color = new Color32(255, 255, 255, 255);
    }

    private void Death()
    {
        if (!isDead)
        {
            isDead = true;
            OnFortressDeath.Invoke();
        }
    }

    private void UpdateDamageView()
    {
        int numberOfStepsInCurrentHealth = (int)(Health / _healthStep);
        _spriteRenderer.sprite = _damageView[_damageView.Count - 1 - numberOfStepsInCurrentHealth];
    }
}
