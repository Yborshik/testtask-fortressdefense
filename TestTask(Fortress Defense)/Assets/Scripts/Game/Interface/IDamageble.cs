﻿/// <summary>
/// Интерфейс получения урона
/// </summary>
public interface IDamageble
{
    float Health { get; }

    void TakeDamage(float damage);
}
