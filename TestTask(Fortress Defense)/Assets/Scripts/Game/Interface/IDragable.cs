﻿using UnityEngine;
/// <summary>
/// Интерфейс перетаскивания игровых обьектов
/// </summary>
public interface IDragable
{
    void InitDrag();
    void Drag(Touch touch);
    void EndDrag();
}
