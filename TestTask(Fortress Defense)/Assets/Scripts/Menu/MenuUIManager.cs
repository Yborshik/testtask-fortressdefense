﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuUIManager : MonoBehaviour {
    public void PlayGame()
    {
        GameSceneManager.Load(SceneName.GAME);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
