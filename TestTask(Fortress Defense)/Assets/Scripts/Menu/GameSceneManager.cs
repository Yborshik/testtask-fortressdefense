﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;

public class GameSceneManager : MonoBehaviour
{
    #region Singleton

    private static GameSceneManager _instance;

    private static GameSceneManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GameSceneManager>();
            if (_instance == null)
            {
                GameObject gameSceneManager = new GameObject("GameSceneManager");
                _instance = gameSceneManager.AddComponent<GameSceneManager>();
            }

            return _instance;
        }
    }
    #endregion

    private void Awake()
    {
        if (_instance == this)
            Destroy(this);
        DontDestroyOnLoad(transform.gameObject);
    }

    public static void Load(string sceneName, Action action = null)
    {
        Instance.StartCoroutine(Instance.IEAsynchronousLoad(sceneName, action));
    }

    /// <summary>
    /// Запускает сцену в фоновом режиме
    /// </summary>
    /// <param name="name">индекс сцены</param>
    private IEnumerator IEAsynchronousLoad(string name, Action action)
    {
        GameObject loadingScreen =  Instantiate(Resources.Load<GameObject>("loadingScreen"), transform);

        AsyncOperation ao = SceneManager.LoadSceneAsync(name);
        ao.allowSceneActivation = false;

        while (!ao.isDone)
        {
            float progress = Mathf.Clamp01(ao.progress / 0.9f);

            if (progress == 1f)
            {
                ao.allowSceneActivation = true;
            }
            yield return new WaitForEndOfFrame();
        }
        action?.Invoke();

        Destroy(loadingScreen);
    }
}

public class SceneName
{
    public const string MENU = "Menu";
    public const string GAME = "Game";
}
